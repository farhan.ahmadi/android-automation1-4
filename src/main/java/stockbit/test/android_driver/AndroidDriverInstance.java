package stockbit.test.android_driver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.remote.DesiredCapabilities;
public class AndroidDriverInstance {
    public static AndroidDriver<AndroidElement>androidDriver;

    public AndroidDriverInstance() {
    }
public static void initialize(){
DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
desiredCapabilities.setCapability("platformName", "Android");
desiredCapabilities.setCapability("platformVersion", "8.1"); //bisa diganti
desiredCapabilities.setCapability("deviceName", "emulator-5554"); //ganti sesuai adb-devices
desiredCapabilities.setCapability("app", "/Users/Farhan Ahmadi/Documents/auto docs/app-development-debug.apk");
desiredCapabilities.setCapability("appWaitActivity", "*");
    try {
        androidDriver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), desiredCapabilities);
        androidDriver.manage().timeouts().implicitlyWait(15L, TimeUnit.SECONDS);
    } catch (MalformedURLException var2) {
        var2.printStackTrace();
    }
}
public static void quit() { androidDriver.quit(); }
}


