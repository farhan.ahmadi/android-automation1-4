package stockbit.test.base;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import stockbit.test.android_driver.AndroidDriverInstance;

public class BasePageObject {
    public BasePageObject(){

    }

    public AndroidDriver driver() {
        return AndroidDriverInstance.androidDriver;
    }

    public void inputText(By element, String text) {
        this.driver().findElement(element).sendKeys(new CharSequence[]{text});
    }

    public void tap(By element) {
        this.driver().findElement(element).click();
    }

    public void isDisplayed(By element) {
        this.driver().findElement(element).isDisplayed();
    }
}







