package step_definition;

import io.cucumber.java8.En;
import stockbit.test.page_object.LoginPage;

public class LoginStep implements En {
    LoginPage loginPage = new LoginPage();
    public LoginStep() {
        Given("^User click entry point login$", () -> {
        });
        When("^User input username \"([^\"]*)\"$", (String password) -> {
        });
        Then("^User input password \"([^\"]*)\"$", (String password) -> {
        });
        And("^User click login button$", () -> {
        });
    }
}
